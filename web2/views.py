from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms

# Create your views here.

def create_status(request):
    if request.method == "POST":
        form = forms.Status_Form(request.POST)
        if form.is_valid():
            #saving form to db
            form.save()

            form = forms.Status_Form()
            context = {'form':form}
            return render(request, 'home.html', context)

    else:
        form = forms.Status_Form()
        context = {'form':form}
        return render(request, 'home.html', context)
