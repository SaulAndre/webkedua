from django.urls import path
from .views import create_status

app_name = 'status'

urlpatterns = [
    path('', create_status, name='status'),
]