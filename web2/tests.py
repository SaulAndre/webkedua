from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import create_status
from .models import Status
from .forms import Status_Form
# Create your tests here.

class Web2Test(TestCase):
    def test_request_web2(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_web2_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_func_web2(self):
        found = resolve('/')
        self.assertEqual(found.func, create_status)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(status="Baik")
        counting_model_content = Status.objects.all().count()
        self.assertEqual(counting_model_content, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'], 
            ['This field is required'])

    def test_form_post_succes_and_render_the_result(self):
        myStatus = 'baik baik saja'
        response = Client().post('/', {'status':''})
        self.assertEqual(response.status_code, 200)
        html_response = response.content_decode('utf8')
        self.assertIn(myStatus, html_response)


    